export interface Product {
    cover: string;
    name: string;
    details: string;
    price?: number;
    discountedPrice?: number;
  }