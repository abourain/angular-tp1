import { Component } from "@angular/core";
import { Product } from "./models/product";


@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"],
})
export class AppComponent {
  title = "tp1";
  products: Product[] = [ 
    {cover:'https://picsum.photos/600/400', name: 'Chaise en Bois', details:'Cette chaise est faite de bois', price: 99, discountedPrice: 399},
    {cover:'https://picsum.photos/600/400', name: 'Comptoir en marbre', details:'Ce comptoir sera parfait pour votre cuisine !'},
    {cover:'https://picsum.photos/600/400', name: 'Réfrigirateur THOMPSON', details:'Réfrigirateur de luxe', price: 1199, discountedPrice: 3999}
  ];
}
