import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ProductItemComponent } from 'src/components/product-item/product-item.component';
import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent,
    ProductItemComponent,
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
